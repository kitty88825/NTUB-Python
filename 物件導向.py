class Animal:
    def __init__(self, name, color):
        self.name = name
        self.color = color
    def say_hello(self):
        print('Hello my name is {}'.format(self.name))

class Dog(Animal):
    def fly(self):
        print('I can fly!(dog)')

# Mixin擴充功能
class FlyMixin:
    def fly(self):
        print('I can fly!')

class Bird(Animal, FlyMixin):
    pass

class FlyDog(Dog, FlyMixin):
    pass

bird1 = Bird('小黑', 'black')
bird1.fly()

print('-'*30)

dog1 = FlyDog('飛飛', 'white')
dog1.fly()