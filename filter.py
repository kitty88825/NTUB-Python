def mod_3_or_5_is_0(x):
    return x % 3 == 0 or x % 5 == 0


target = list(range(1, 101))

result = filter(mod_3_or_5_is_0, target)
print(list(result))