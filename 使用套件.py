from terminaltables import AsciiTable

data = [
    ["col 1", "col 1", "col 1"],
    [1, 2, 3],
    [1, 2, 3],
    [1, 2, 3],
    [1, 2, 3],
]

table = AsciiTable(data)
print(table.table)