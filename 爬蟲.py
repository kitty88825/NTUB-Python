import csv
import requests

from bs4 import BeautifulSoup

res = requests.get('http://www.ntub.edu.tw/files/501-1000-1003.php')
if not res.ok:
    print('Request fail!')
    exit()

res.encoding = 'utf-8'
soup = BeautifulSoup(res.text, 'html.parser')

result = []
news = soup.select('div.h5')
for i in news:
    link = i.select_one('a')
    date = i.select_one('span.date')

    title = link.text
    url = link['href']
    date = date.text.replace('[', '').replace(']', '').strip()

    result.append([title, date, url])


with open('ntub-new.csv', 'w', encoding='utf-8', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['title', 'date', 'url'])
    writer.writerows(result)

print('Done~~~')