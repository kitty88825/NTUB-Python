from functools import reduce

def xxx(x, y):
    return x + y

target = list(range(1, 6))
result = reduce(xxx, target)

print(result)